import 'styled-components';

import { createGlobalStyle } from 'styled-components/macro';

const BaseCSS = createGlobalStyle` 

  // general init styles
  html{ 
    scroll-behavior: smooth;
    margin: 0;
    padding: 0;
  }
  body{
    margin: 0;
    padding: 0;
  }
  *{box-sizing: border-box}

`
const FunkyCSS = createGlobalStyle`  
  
  // general styles
  *{
    /* outline: 1px dashed #ffd5d5 !important;  */
  }


  body{
    border-radius: 2px; 
  }

  p{ line-height: 1.5;} 

  img{max-width: 100%}
`

const GeneralCSS = createGlobalStyle` 
  body{
    ${({theme: {text}}) => text?.style1} 
    background: ${({ theme: {bg}  }) => bg?.main }; 
  }
`

export {BaseCSS, FunkyCSS, GeneralCSS}