import { Theme } from '@material-ui/core/styles';
import {amber} from '@material-ui/core/colors'
import { createTheme } from "@material-ui/core";

interface CustomTheme {
    bg?: {
        main?: string
    }, 
    text?: {
        main?: string,
        style1?: string,
        hightlight?: string,
    },
    flex?: {
      row?: string
    }
}
declare module '@material-ui/core/styles' {
    interface Theme extends CustomTheme {}
    interface ThemeOptions extends CustomTheme {}
}

declare module 'styled-components' {
    export interface DefaultTheme extends Theme {}
}



const fontfamilyArray = [
  "'Azeret Mono', monospace",
  "'Yellowtail', cursive;",
  "'Roboto', sans-serif;",
  "'Cabin', sans-serif;",
  // "'proggy', monospace"s,
]


const themeFunction = (font: number, color: number) => {

  let background = ['#fdfbf8', '#111'];
  let textColor = ['#1e272e', '#eee'] 
  const fontSize = [14, 14, 14, 22]
  const hightlight = ['#ffa', '#880']

  return createTheme({
    typography: {
      "fontFamily": `${fontfamilyArray[font]}`,
      "fontSize": fontSize[font],
    },
    flex: {
      row: `display: flex;
      flex-direction: row;`
    },
    bg: {
      main: background[color]
    },
    text: {
      main: textColor[color],
      style1: `font-family: ${fontfamilyArray[font]};
      font-size: ${fontSize[font]}px;
      color: ${textColor[color]};
      font-weight: 600;
      opacity: 0.8;`,
      hightlight: hightlight[color]
    }
  })
}




export {themeFunction}