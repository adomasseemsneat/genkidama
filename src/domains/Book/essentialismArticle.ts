import { dynamic, dynamicTest } from "scripts"

import { ScriptTag } from "comps"

let visual = ''

const result = {
  names: ['result', 'value to the world', 'value to society'],
  constituents: `productivity /dx`
  
}

// footnotes
const note1 = result
const note2 = ''
const ref1 = `The same amount of energy, when directed purposefully, can bring you much closer to your goal.${note2} Know the right things you must do, reject everything else and direct your time and energy to create the biggest impact where it matters.`














let essentialismArticle = {
  edited: `Essentialism is fundamentally about designing and ${`taking control of your life` || note1}. ${ref1} In doing ${`so`||ref1}, you drastically improve both your levels of performance and results.`, 


  original: `Essentialism is fundamentally about designing and taking control of your life. The same amount of energy, when directed purposefully, can bring you much closer to your goal. Know the right things you must do, reject everything else and direct your time and energy to create the biggest impact where it matters. In so doing, you drastically improve both your levels of performance and results.`
}


const essentialism = {
  name: 'Essentialism',
  details: 'indre sucks in the morning on imaginary peners',
  alt: ['deep work /thinking / booredom'],
}



const improve = {
  name: 'decrease Variability of life'
}


const f = [
  {text: 'from the book'},
  // improve
]

let EssentialArticle: any = []

const EssentialArticle1 = dynamic(ScriptTag)`
${   [ `Essentialism`, f[0] ]  } is fundamentally about designing and taking control of your life. The same amount of energy, when directed purposefully, can bring you much closer to your goal. Know the right things you must do, reject everything else and direct your time and energy to create the biggest impact where it matters. In so doing, you drastically improve both your levels of performance and results.
`




const EssentialArticle2 = dynamic(ScriptTag)`
  Essentialism is fundamentally about designing and taking control of your life .
`

 


export {essentialismArticle, EssentialArticle1, EssentialArticle2}