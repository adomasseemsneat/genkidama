import { EssentialArticle1, EssentialArticle2 } from "./essentialismArticle"

import { MyContainer } from "comps"
import { ScriptTag } from "comps"
import { dynamic } from 'scripts/dynamic'
import { mainText } from "./text"

const Book = () => { 


  return (
    <MyContainer>
      <EssentialArticle1 />
      <EssentialArticle2 />
    </MyContainer>
  )
}

export default Book
